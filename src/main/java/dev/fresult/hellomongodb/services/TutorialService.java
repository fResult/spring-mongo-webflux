package dev.fresult.hellomongodb.services;

import dev.fresult.hellomongodb.models.Tutorial;
import dev.fresult.hellomongodb.repositories.TutorialRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TutorialService {
  private final TutorialRepository repo;

  public Flux<Tutorial> all() {
    return repo.findAll();
  }

  public Flux<Tutorial> byTitleContaining(String title) {
    return repo.findByTitleContaining(title);
  }

  public Mono<Tutorial> byId(String id) {
    return repo.findById(id);
  }

  public Mono<Tutorial> create(Tutorial tutorial) {
    tutorial.clone();
    return repo.save(tutorial);
  }

  public Mono<Tutorial> update(String id, Tutorial tutorial) {
    return repo.findById(id).map(Optional::of).defaultIfEmpty(Optional.empty())
        .flatMap(tutorialOpt -> {
          if (tutorialOpt.isPresent()) {
            tutorial.setId(id);
            return repo.save(tutorial);
          }
          return Mono.empty();
        });
  }

  public Mono<Void> deleteById(String id) {
    return repo.deleteById(id);
  }

  public Mono<Void> deleteAll() {
    return repo.deleteAll();
  }

//  public Function<Boolean, Flux<Tutorial>> byPublished = repo::findByPublished;

  public Flux<Tutorial> byPublished(boolean published) {
    return repo.findByPublished(published);
  }

}
