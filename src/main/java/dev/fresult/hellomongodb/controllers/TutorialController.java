package dev.fresult.hellomongodb.controllers;

import dev.fresult.hellomongodb.models.Tutorial;
import dev.fresult.hellomongodb.services.TutorialService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/tutorials")
@RequiredArgsConstructor
public class TutorialController {
  private final TutorialService service;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public Flux<Tutorial> all(@RequestParam(required = false) String title) {
    System.out.println("Find All " + title);
    if (title == null) {
      return service.all();
    }
    return service.byTitleContaining(title);
  }

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public Mono<Tutorial> byId(@PathVariable String id) {
    return service.byId(id);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Tutorial> create(@RequestBody Tutorial tutorial) {
    return service.create(tutorial);
  }

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public Mono<Tutorial> update(@PathVariable String id, Tutorial tutorial) {
    return service.update(id, tutorial);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public Mono<Void> deleleteById(@PathVariable String id) {
    return service.deleteById(id);
  }

  @DeleteMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public Mono<Void> deleteAll() {
    return service.deleteAll();
  }

  @GetMapping("/published")
  @ResponseStatus(HttpStatus.OK)
  public Flux<Tutorial> byPublished() {
    return service.byPublished(true);
  }

}
