package dev.fresult.hellomongodb.models;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@RequiredArgsConstructor
public class Tutorial implements Cloneable {
  private @Id String id;
  private String title;
  private String description = null;
  private boolean published = false;

  @Override
  public Tutorial clone() {
    try {
      // TODO: copy mutable state here, so the clone can't change the internals of the original
      return (Tutorial) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new AssertionError();
    }
  }
}

//@Document
//public record Tutorial(
//    @Id String id,
//    String title,
//    String description,
//    boolean published) {
//}
