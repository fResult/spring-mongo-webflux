package dev.fresult.hellomongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableWebFlux
//@EnableMongoRepositories
public class HelloMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloMongodbApplication.class, args);
	}

}
